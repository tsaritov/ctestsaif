# testSaif

Create an Facebook Instant Game which can be run in test mode.
Game should have one screen. On the screen should be  displayed avatar of user who run 
the game and list of items  witch  get  by request url "http://95.217.161.159:3000/".
---
 On screen  should been displayed 
 - title
 - description
 - icon.   
---
**Technical Requirements**
- Webpack for build
- App build in zip file  ready for upload  to facebook
- Facebook application keys in config file


---

The main success criterion is:
 - the ability to specify keys
 - run a build
 - upload the build to facebook
 - run a game
 - get a result that matches the description of the task
